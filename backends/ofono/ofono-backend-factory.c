/* ofono-backend-factory.c generated by valac 0.35.3.10-6b27, the Vala compiler
 * generated from ofono-backend-factory.vala, do not modify */

/*
 * Copyright (C) 2009 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 * Copyright (C) 2009 Nokia Corporation.
 * Copyright (C) 2012 Collabora Ltd.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *          Jeremy Whiting <jeremy.whiting@collabora.co.uk>
 *
 * Based on kf-backend-factory.vala by:
 *          Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *          Travis Reitter <travis.reitter@collabora.co.uk>
 *          Philip Withnall <philip.withnall@collabora.co.uk>
 */

#include <glib.h>
#include <glib-object.h>
#include <folks/folks.h>


#define FOLKS_BACKENDS_OFONO_TYPE_BACKEND (folks_backends_ofono_backend_get_type ())
#define FOLKS_BACKENDS_OFONO_BACKEND(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), FOLKS_BACKENDS_OFONO_TYPE_BACKEND, FolksBackendsOfonoBackend))
#define FOLKS_BACKENDS_OFONO_BACKEND_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), FOLKS_BACKENDS_OFONO_TYPE_BACKEND, FolksBackendsOfonoBackendClass))
#define FOLKS_BACKENDS_OFONO_IS_BACKEND(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), FOLKS_BACKENDS_OFONO_TYPE_BACKEND))
#define FOLKS_BACKENDS_OFONO_IS_BACKEND_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), FOLKS_BACKENDS_OFONO_TYPE_BACKEND))
#define FOLKS_BACKENDS_OFONO_BACKEND_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), FOLKS_BACKENDS_OFONO_TYPE_BACKEND, FolksBackendsOfonoBackendClass))

typedef struct _FolksBackendsOfonoBackend FolksBackendsOfonoBackend;
typedef struct _FolksBackendsOfonoBackendClass FolksBackendsOfonoBackendClass;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))



void module_init (FolksBackendStore* backend_store);
FolksBackendsOfonoBackend* folks_backends_ofono_backend_new (void);
FolksBackendsOfonoBackend* folks_backends_ofono_backend_construct (GType object_type);
GType folks_backends_ofono_backend_get_type (void) G_GNUC_CONST;
void module_finalize (FolksBackendStore* backend_store);


/**
 * The backend module entry point.
 *
 * @param backend_store the {@link BackendStore} to use in this factory.
 *
 * @since 0.9.0
 */
void module_init (FolksBackendStore* backend_store) {
	FolksBackendStore* _tmp0_;
	FolksBackendsOfonoBackend* _tmp1_;
	FolksBackendsOfonoBackend* _tmp2_;
	g_return_if_fail (backend_store != NULL);
	_tmp0_ = backend_store;
	_tmp1_ = folks_backends_ofono_backend_new ();
	_tmp2_ = _tmp1_;
	folks_backend_store_add_backend (_tmp0_, (FolksBackend*) _tmp2_);
	_g_object_unref0 (_tmp2_);
}


/**
 * The backend module exit point.
 *
 * @param backend_store the {@link BackendStore} used in this factory.
 *
 * @since 0.9.0
 */
void module_finalize (FolksBackendStore* backend_store) {
	g_return_if_fail (backend_store != NULL);
}



