/* birthday-details.c generated by valac 0.36.1.697-2b74, the Vala compiler
 * generated from birthday-details.vala, do not modify */

/*
 * Copyright (C) 2011 Collabora Ltd.
 * Copyright (C) 2011 Philip Withnall
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Raul Gutierrez Segales <raul.gutierrez.segales@collabora.co.uk>
 *       Philip Withnall <philip@tecnocode.co.uk>
 */

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <stdlib.h>
#include <string.h>
#include <glib/gi18n-lib.h>


#define FOLKS_TYPE_BIRTHDAY_DETAILS (folks_birthday_details_get_type ())
#define FOLKS_BIRTHDAY_DETAILS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), FOLKS_TYPE_BIRTHDAY_DETAILS, FolksBirthdayDetails))
#define FOLKS_IS_BIRTHDAY_DETAILS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), FOLKS_TYPE_BIRTHDAY_DETAILS))
#define FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), FOLKS_TYPE_BIRTHDAY_DETAILS, FolksBirthdayDetailsIface))

typedef struct _FolksBirthdayDetails FolksBirthdayDetails;
typedef struct _FolksBirthdayDetailsIface FolksBirthdayDetailsIface;
#define _g_date_time_unref0(var) ((var == NULL) ? NULL : (var = (g_date_time_unref (var), NULL)))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
typedef struct _FolksBirthdayDetailsChangeBirthdayData FolksBirthdayDetailsChangeBirthdayData;
#define _g_free0(var) (var = (g_free (var), NULL))
typedef struct _FolksBirthdayDetailsChangeCalendarEventIdData FolksBirthdayDetailsChangeCalendarEventIdData;

typedef enum  {
	FOLKS_PROPERTY_ERROR_NOT_WRITEABLE,
	FOLKS_PROPERTY_ERROR_INVALID_VALUE,
	FOLKS_PROPERTY_ERROR_UNKNOWN_ERROR,
	FOLKS_PROPERTY_ERROR_UNAVAILABLE
} FolksPropertyError;
#define FOLKS_PROPERTY_ERROR folks_property_error_quark ()
struct _FolksBirthdayDetailsIface {
	GTypeInterface parent_iface;
	void (*change_birthday) (FolksBirthdayDetails* self, GDateTime* birthday, GAsyncReadyCallback _callback_, gpointer _user_data_);
	void (*change_birthday_finish) (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error);
	void (*change_calendar_event_id) (FolksBirthdayDetails* self, const gchar* event_id, GAsyncReadyCallback _callback_, gpointer _user_data_);
	void (*change_calendar_event_id_finish) (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error);
	GDateTime* (*get_birthday) (FolksBirthdayDetails* self);
	void (*set_birthday) (FolksBirthdayDetails* self, GDateTime* value);
	const gchar* (*get_calendar_event_id) (FolksBirthdayDetails* self);
	void (*set_calendar_event_id) (FolksBirthdayDetails* self, const gchar* value);
};

struct _FolksBirthdayDetailsChangeBirthdayData {
	int _state_;
	GObject* _source_object_;
	GAsyncResult* _res_;
	GTask* _async_result;
	GAsyncReadyCallback _callback_;
	gboolean _task_complete_;
	FolksBirthdayDetails* self;
	GDateTime* birthday;
	GError* _tmp0_;
	GError * _inner_error_;
};

struct _FolksBirthdayDetailsChangeCalendarEventIdData {
	int _state_;
	GObject* _source_object_;
	GAsyncResult* _res_;
	GTask* _async_result;
	GAsyncReadyCallback _callback_;
	gboolean _task_complete_;
	FolksBirthdayDetails* self;
	gchar* event_id;
	GError* _tmp0_;
	GError * _inner_error_;
};



GQuark folks_property_error_quark (void);
GType folks_birthday_details_get_type (void) G_GNUC_CONST;
static void folks_birthday_details_real_change_birthday_data_free (gpointer _data);
static void folks_birthday_details_real_change_birthday_async_ready_wrapper (GObject *source_object, GAsyncResult *res, void *user_data);
static void folks_birthday_details_real_change_birthday (FolksBirthdayDetails* self, GDateTime* birthday, GAsyncReadyCallback _callback_, gpointer _user_data_);
void folks_birthday_details_change_birthday (FolksBirthdayDetails* self, GDateTime* birthday, GAsyncReadyCallback _callback_, gpointer _user_data_);
void folks_birthday_details_change_birthday_finish (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error);
static gboolean folks_birthday_details_real_change_birthday_co (FolksBirthdayDetailsChangeBirthdayData* _data_);
static void folks_birthday_details_real_change_calendar_event_id_data_free (gpointer _data);
static void folks_birthday_details_real_change_calendar_event_id_async_ready_wrapper (GObject *source_object, GAsyncResult *res, void *user_data);
static void folks_birthday_details_real_change_calendar_event_id (FolksBirthdayDetails* self, const gchar* event_id, GAsyncReadyCallback _callback_, gpointer _user_data_);
void folks_birthday_details_change_calendar_event_id (FolksBirthdayDetails* self, const gchar* event_id, GAsyncReadyCallback _callback_, gpointer _user_data_);
void folks_birthday_details_change_calendar_event_id_finish (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error);
static gboolean folks_birthday_details_real_change_calendar_event_id_co (FolksBirthdayDetailsChangeCalendarEventIdData* _data_);
GDateTime* folks_birthday_details_get_birthday (FolksBirthdayDetails* self);
void folks_birthday_details_set_birthday (FolksBirthdayDetails* self, GDateTime* value);
const gchar* folks_birthday_details_get_calendar_event_id (FolksBirthdayDetails* self);
void folks_birthday_details_set_calendar_event_id (FolksBirthdayDetails* self, const gchar* value);


static void folks_birthday_details_real_change_birthday_data_free (gpointer _data) {
	FolksBirthdayDetailsChangeBirthdayData* _data_;
	_data_ = _data;
	_g_date_time_unref0 (_data_->birthday);
	_g_object_unref0 (_data_->self);
	g_slice_free (FolksBirthdayDetailsChangeBirthdayData, _data_);
}


static void folks_birthday_details_real_change_birthday_async_ready_wrapper (GObject *source_object, GAsyncResult *res, void *user_data) {
	FolksBirthdayDetailsChangeBirthdayData* _task_data_;
	_task_data_ = g_task_get_task_data (G_TASK (res));
	if (_task_data_->_callback_ != NULL) {
		_task_data_->_callback_ (source_object, res, user_data);
	}
	_task_data_->_task_complete_ = TRUE;
}


static gpointer _g_object_ref0 (gpointer self) {
	return self ? g_object_ref (self) : NULL;
}


static gpointer _g_date_time_ref0 (gpointer self) {
	return self ? g_date_time_ref (self) : NULL;
}


static void folks_birthday_details_real_change_birthday (FolksBirthdayDetails* self, GDateTime* birthday, GAsyncReadyCallback _callback_, gpointer _user_data_) {
	FolksBirthdayDetailsChangeBirthdayData* _data_;
	FolksBirthdayDetails* _tmp0_;
	GDateTime* _tmp1_;
	GDateTime* _tmp2_;
	_data_ = g_slice_new0 (FolksBirthdayDetailsChangeBirthdayData);
	_data_->_callback_ = _callback_;
	_data_->_async_result = g_task_new (G_OBJECT (self), NULL, folks_birthday_details_real_change_birthday_async_ready_wrapper, _user_data_);
	if (_callback_ == NULL) {
		_data_->_task_complete_ = TRUE;
	}
	g_task_set_task_data (_data_->_async_result, _data_, folks_birthday_details_real_change_birthday_data_free);
	_tmp0_ = _g_object_ref0 (self);
	_data_->self = _tmp0_;
	_tmp1_ = birthday;
	_tmp2_ = _g_date_time_ref0 (_tmp1_);
	_g_date_time_unref0 (_data_->birthday);
	_data_->birthday = _tmp2_;
	folks_birthday_details_real_change_birthday_co (_data_);
}


static void folks_birthday_details_real_change_birthday_finish (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error) {
	FolksBirthdayDetailsChangeBirthdayData* _data_;
	_data_ = g_task_propagate_pointer (G_TASK (_res_), error);
	if (NULL == _data_) {
		return;
	}
}


/**
   * Change the contact's birthday.
   *
   * It's preferred to call this rather than setting
   * {@link BirthdayDetails.birthday} directly, as this method gives error
   * notification and will only return once the birthday has been written to the
   * relevant backing store (or the operation's failed).
   *
   * @param birthday the new birthday (or ``null`` to unset the birthday)
   * @throws PropertyError if setting the birthday failed
   * @since 0.6.2
   */
static gboolean folks_birthday_details_real_change_birthday_co (FolksBirthdayDetailsChangeBirthdayData* _data_) {
	switch (_data_->_state_) {
		case 0:
		goto _state_0;
		default:
		g_assert_not_reached ();
	}
	_state_0:
	_data_->_tmp0_ = g_error_new_literal (FOLKS_PROPERTY_ERROR, FOLKS_PROPERTY_ERROR_NOT_WRITEABLE, _ ("Birthday is not writeable on this contact."));
	_data_->_inner_error_ = _data_->_tmp0_;
	if (_data_->_inner_error_->domain == FOLKS_PROPERTY_ERROR) {
		g_task_return_error (_data_->_async_result, _data_->_inner_error_);
		g_object_unref (_data_->_async_result);
		return FALSE;
	} else {
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _data_->_inner_error_->message, g_quark_to_string (_data_->_inner_error_->domain), _data_->_inner_error_->code);
		g_clear_error (&_data_->_inner_error_);
		g_object_unref (_data_->_async_result);
		return FALSE;
	}
	g_task_return_pointer (_data_->_async_result, _data_, NULL);
	if (_data_->_state_ != 0) {
		while (_data_->_task_complete_ != TRUE) {
			g_main_context_iteration (g_task_get_context (_data_->_async_result), TRUE);
		}
	}
	g_object_unref (_data_->_async_result);
	return FALSE;
}


void folks_birthday_details_change_birthday (FolksBirthdayDetails* self, GDateTime* birthday, GAsyncReadyCallback _callback_, gpointer _user_data_) {
	FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->change_birthday (self, birthday, _callback_, _user_data_);
}


void folks_birthday_details_change_birthday_finish (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error) {
	FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->change_birthday_finish (self, _res_, error);
}


static void folks_birthday_details_real_change_calendar_event_id_data_free (gpointer _data) {
	FolksBirthdayDetailsChangeCalendarEventIdData* _data_;
	_data_ = _data;
	_g_free0 (_data_->event_id);
	_g_object_unref0 (_data_->self);
	g_slice_free (FolksBirthdayDetailsChangeCalendarEventIdData, _data_);
}


static void folks_birthday_details_real_change_calendar_event_id_async_ready_wrapper (GObject *source_object, GAsyncResult *res, void *user_data) {
	FolksBirthdayDetailsChangeCalendarEventIdData* _task_data_;
	_task_data_ = g_task_get_task_data (G_TASK (res));
	if (_task_data_->_callback_ != NULL) {
		_task_data_->_callback_ (source_object, res, user_data);
	}
	_task_data_->_task_complete_ = TRUE;
}


static void folks_birthday_details_real_change_calendar_event_id (FolksBirthdayDetails* self, const gchar* event_id, GAsyncReadyCallback _callback_, gpointer _user_data_) {
	FolksBirthdayDetailsChangeCalendarEventIdData* _data_;
	FolksBirthdayDetails* _tmp0_;
	const gchar* _tmp1_;
	gchar* _tmp2_;
	_data_ = g_slice_new0 (FolksBirthdayDetailsChangeCalendarEventIdData);
	_data_->_callback_ = _callback_;
	_data_->_async_result = g_task_new (G_OBJECT (self), NULL, folks_birthday_details_real_change_calendar_event_id_async_ready_wrapper, _user_data_);
	if (_callback_ == NULL) {
		_data_->_task_complete_ = TRUE;
	}
	g_task_set_task_data (_data_->_async_result, _data_, folks_birthday_details_real_change_calendar_event_id_data_free);
	_tmp0_ = _g_object_ref0 (self);
	_data_->self = _tmp0_;
	_tmp1_ = event_id;
	_tmp2_ = g_strdup (_tmp1_);
	_g_free0 (_data_->event_id);
	_data_->event_id = _tmp2_;
	folks_birthday_details_real_change_calendar_event_id_co (_data_);
}


static void folks_birthday_details_real_change_calendar_event_id_finish (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error) {
	FolksBirthdayDetailsChangeCalendarEventIdData* _data_;
	_data_ = g_task_propagate_pointer (G_TASK (_res_), error);
	if (NULL == _data_) {
		return;
	}
}


/**
   * Change the contact's birthday event ID.
   *
   * It's preferred to call this rather than setting
   * {@link BirthdayDetails.calendar_event_id} directly, as this method gives
   * error notification and will only return once the event has been written to
   * the relevant backing store (or the operation's failed).
   *
   * @param event_id the new birthday event ID (or ``null`` to unset the event
   * ID)
   * @throws PropertyError if setting the birthday event ID failed
   * @since 0.6.2
   */
static gboolean folks_birthday_details_real_change_calendar_event_id_co (FolksBirthdayDetailsChangeCalendarEventIdData* _data_) {
	switch (_data_->_state_) {
		case 0:
		goto _state_0;
		default:
		g_assert_not_reached ();
	}
	_state_0:
	_data_->_tmp0_ = g_error_new_literal (FOLKS_PROPERTY_ERROR, FOLKS_PROPERTY_ERROR_NOT_WRITEABLE, _ ("Birthday event ID is not writeable on this contact."));
	_data_->_inner_error_ = _data_->_tmp0_;
	if (_data_->_inner_error_->domain == FOLKS_PROPERTY_ERROR) {
		g_task_return_error (_data_->_async_result, _data_->_inner_error_);
		g_object_unref (_data_->_async_result);
		return FALSE;
	} else {
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _data_->_inner_error_->message, g_quark_to_string (_data_->_inner_error_->domain), _data_->_inner_error_->code);
		g_clear_error (&_data_->_inner_error_);
		g_object_unref (_data_->_async_result);
		return FALSE;
	}
	g_task_return_pointer (_data_->_async_result, _data_, NULL);
	if (_data_->_state_ != 0) {
		while (_data_->_task_complete_ != TRUE) {
			g_main_context_iteration (g_task_get_context (_data_->_async_result), TRUE);
		}
	}
	g_object_unref (_data_->_async_result);
	return FALSE;
}


void folks_birthday_details_change_calendar_event_id (FolksBirthdayDetails* self, const gchar* event_id, GAsyncReadyCallback _callback_, gpointer _user_data_) {
	FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->change_calendar_event_id (self, event_id, _callback_, _user_data_);
}


void folks_birthday_details_change_calendar_event_id_finish (FolksBirthdayDetails* self, GAsyncResult* _res_, GError** error) {
	FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->change_calendar_event_id_finish (self, _res_, error);
}


GDateTime* folks_birthday_details_get_birthday (FolksBirthdayDetails* self) {
	g_return_val_if_fail (self != NULL, NULL);
	return FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->get_birthday (self);
}


void folks_birthday_details_set_birthday (FolksBirthdayDetails* self, GDateTime* value) {
	g_return_if_fail (self != NULL);
	FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->set_birthday (self, value);
}


const gchar* folks_birthday_details_get_calendar_event_id (FolksBirthdayDetails* self) {
	g_return_val_if_fail (self != NULL, NULL);
	return FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->get_calendar_event_id (self);
}


void folks_birthday_details_set_calendar_event_id (FolksBirthdayDetails* self, const gchar* value) {
	g_return_if_fail (self != NULL);
	FOLKS_BIRTHDAY_DETAILS_GET_INTERFACE (self)->set_calendar_event_id (self, value);
}


static void folks_birthday_details_base_init (FolksBirthdayDetailsIface * iface) {
	static gboolean initialized = FALSE;
	if (!initialized) {
		initialized = TRUE;
		/**
		   * The birthday of the {@link Persona} and {@link Individual}. This
		   * is assumed to be in UTC.
		   *
		   * If this is ``null``, the contact's birthday isn't known.
		   *
		   * @since 0.4.0
		   */
		g_object_interface_install_property (iface, g_param_spec_boxed ("birthday", "birthday", "birthday", G_TYPE_DATE_TIME, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
		/**
		   * The event ID of the birthday event from the source calendar.
		   *
		   * If this is ``null``, the birthday event is unknown. The semantics of the
		   * event ID are left unspecified by folks.
		   *
		   * @since 0.4.0
		   */
		g_object_interface_install_property (iface, g_param_spec_string ("calendar-event-id", "calendar-event-id", "calendar-event-id", NULL, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
		iface->change_birthday = folks_birthday_details_real_change_birthday;
		iface->change_birthday_finish = folks_birthday_details_real_change_birthday_finish;
		iface->change_calendar_event_id = folks_birthday_details_real_change_calendar_event_id;
		iface->change_calendar_event_id_finish = folks_birthday_details_real_change_calendar_event_id_finish;
	}
}


/**
 * Birthday details for a contact.
 *
 * This allows representation of the birth date and associated calendar event ID
 * of a contact.
 *
 * @since 0.4.0
 */
GType folks_birthday_details_get_type (void) {
	static volatile gsize folks_birthday_details_type_id__volatile = 0;
	if (g_once_init_enter (&folks_birthday_details_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (FolksBirthdayDetailsIface), (GBaseInitFunc) folks_birthday_details_base_init, (GBaseFinalizeFunc) NULL, (GClassInitFunc) NULL, (GClassFinalizeFunc) NULL, NULL, 0, 0, (GInstanceInitFunc) NULL, NULL };
		GType folks_birthday_details_type_id;
		folks_birthday_details_type_id = g_type_register_static (G_TYPE_INTERFACE, "FolksBirthdayDetails", &g_define_type_info, 0);
		g_type_interface_add_prerequisite (folks_birthday_details_type_id, G_TYPE_OBJECT);
		g_once_init_leave (&folks_birthday_details_type_id__volatile, folks_birthday_details_type_id);
	}
	return folks_birthday_details_type_id__volatile;
}



