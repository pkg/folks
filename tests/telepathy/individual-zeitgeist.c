/* individual-zeitgeist.c generated by valac 0.35.3.10-6b27, the Vala compiler
 * generated from individual-zeitgeist.vala, do not modify */

/*
 * Copyright (C) 2012 Collabora
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Seif Lotfy <seif.lotfy@collabora.co.uk>
 */

#include <glib.h>
#include <glib-object.h>
#include <folks/folks.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gee.h>
#include <gio/gio.h>

#define _g_date_time_unref0(var) ((var == NULL) ? NULL : (var = (g_date_time_unref (var), NULL)))
typedef struct _Block1Data Block1Data;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
typedef struct _Block2Data Block2Data;
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))
#define _g_main_loop_unref0(var) ((var == NULL) ? NULL : (var = (g_main_loop_unref (var), NULL)))

struct _Block1Data {
	int _ref_count_;
	FolksIndividualAggregator* aggregator;
};

struct _Block2Data {
	int _ref_count_;
	Block1Data * _data1_;
	FolksIndividual* i;
};



void print_individual (FolksIndividual* i);
gint _vala_main (gchar** args, int args_length1);
static Block1Data* block1_data_ref (Block1Data* _data1_);
static void block1_data_unref (void * _userdata_);
static void __lambda4_ (Block1Data* _data1_);
static Block2Data* block2_data_ref (Block2Data* _data2_);
static void block2_data_unref (void * _userdata_);
static void ____lambda5_ (Block2Data* _data2_);
static void _____lambda5__g_object_notify (GObject* _sender, GParamSpec* pspec, gpointer self);
static void ____lambda6_ (Block2Data* _data2_);
static void _____lambda6__g_object_notify (GObject* _sender, GParamSpec* pspec, gpointer self);
static void ___lambda4__g_object_notify (GObject* _sender, GParamSpec* pspec, gpointer self);
static gboolean __lambda7_ (Block1Data* _data1_);
static void __lambda8_ (Block1Data* _data1_, GObject* s, GAsyncResult* r);
static void ___lambda8__gasync_ready_callback (GObject* source_object, GAsyncResult* res, gpointer self);
static gboolean ___lambda7__gsource_func (gpointer self);


static gpointer _g_date_time_ref0 (gpointer self) {
#line 31 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	return self ? g_date_time_ref (self) : NULL;
#line 74 "individual-zeitgeist.c"
}


void print_individual (FolksIndividual* i) {
	guint new_count;
	FolksIndividual* _tmp0_;
	guint _tmp1_;
	guint _tmp2_;
	guint new_call_count;
	FolksIndividual* _tmp3_;
	guint _tmp4_;
	guint _tmp5_;
	GDateTime* new_call_datetime;
	FolksIndividual* _tmp6_;
	GDateTime* _tmp7_;
	GDateTime* _tmp8_;
	GDateTime* _tmp9_;
	GDateTime* new_chat_datetime;
	FolksIndividual* _tmp10_;
	GDateTime* _tmp11_;
	GDateTime* _tmp12_;
	GDateTime* _tmp13_;
	guint _tmp14_;
#line 27 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	g_return_if_fail (i != NULL);
#line 29 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp0_ = i;
#line 29 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp1_ = folks_interaction_details_get_im_interaction_count ((FolksInteractionDetails*) _tmp0_);
#line 29 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp2_ = _tmp1_;
#line 29 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	new_count = _tmp2_;
#line 30 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp3_ = i;
#line 30 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp4_ = folks_interaction_details_get_call_interaction_count ((FolksInteractionDetails*) _tmp3_);
#line 30 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp5_ = _tmp4_;
#line 30 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	new_call_count = _tmp5_;
#line 31 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp6_ = i;
#line 31 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp7_ = folks_interaction_details_get_last_call_interaction_datetime ((FolksInteractionDetails*) _tmp6_);
#line 31 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp8_ = _tmp7_;
#line 31 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp9_ = _g_date_time_ref0 (_tmp8_);
#line 31 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	new_call_datetime = _tmp9_;
#line 32 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp10_ = i;
#line 32 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp11_ = folks_interaction_details_get_last_im_interaction_datetime ((FolksInteractionDetails*) _tmp10_);
#line 32 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp12_ = _tmp11_;
#line 32 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp13_ = _g_date_time_ref0 (_tmp12_);
#line 32 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	new_chat_datetime = _tmp13_;
#line 33 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp14_ = new_count;
#line 33 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	if (_tmp14_ > ((guint) 0)) {
#line 140 "individual-zeitgeist.c"
		gint64 timestamp;
		GDateTime* _tmp15_;
		FILE* _tmp18_;
		FolksIndividual* _tmp19_;
		const gchar* _tmp20_;
		const gchar* _tmp21_;
		guint _tmp22_;
		gint64 _tmp23_;
		GDateTime* _tmp24_;
		FILE* _tmp27_;
		guint _tmp28_;
		gint64 _tmp29_;
#line 35 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		timestamp = (gint64) 0;
#line 36 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp15_ = new_chat_datetime;
#line 36 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		if (_tmp15_ != NULL) {
#line 159 "individual-zeitgeist.c"
			GDateTime* _tmp16_;
			gint64 _tmp17_;
#line 37 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp16_ = new_chat_datetime;
#line 37 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp17_ = g_date_time_to_unix (_tmp16_);
#line 37 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			timestamp = _tmp17_;
#line 168 "individual-zeitgeist.c"
		}
#line 38 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp18_ = stdout;
#line 38 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp19_ = i;
#line 38 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp20_ = folks_alias_details_get_alias ((FolksAliasDetails*) _tmp19_);
#line 38 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp21_ = _tmp20_;
#line 38 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp22_ = new_count;
#line 38 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp23_ = timestamp;
#line 38 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		fprintf (_tmp18_, "\n" \
" %s\n" \
"   chat interaction count: %u\n" \
"   chat interaction timestamp: %lld\n", _tmp21_, _tmp22_, _tmp23_);
#line 39 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		timestamp = (gint64) 0;
#line 40 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp24_ = new_call_datetime;
#line 40 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		if (_tmp24_ != NULL) {
#line 190 "individual-zeitgeist.c"
			GDateTime* _tmp25_;
			gint64 _tmp26_;
#line 41 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp25_ = new_call_datetime;
#line 41 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp26_ = g_date_time_to_unix (_tmp25_);
#line 41 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			timestamp = _tmp26_;
#line 199 "individual-zeitgeist.c"
		}
#line 42 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp27_ = stdout;
#line 42 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp28_ = new_call_count;
#line 42 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp29_ = timestamp;
#line 42 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		fprintf (_tmp27_, "   call interaction count: %u\n   call interaction timestamp: %lld\n", _tmp28_, _tmp29_);
#line 209 "individual-zeitgeist.c"
	}
#line 27 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_g_date_time_unref0 (new_chat_datetime);
#line 27 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_g_date_time_unref0 (new_call_datetime);
#line 215 "individual-zeitgeist.c"
}


static Block1Data* block1_data_ref (Block1Data* _data1_) {
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	g_atomic_int_inc (&_data1_->_ref_count_);
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	return _data1_;
#line 224 "individual-zeitgeist.c"
}


static void block1_data_unref (void * _userdata_) {
	Block1Data* _data1_;
	_data1_ = (Block1Data*) _userdata_;
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	if (g_atomic_int_dec_and_test (&_data1_->_ref_count_)) {
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_g_object_unref0 (_data1_->aggregator);
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		g_slice_free (Block1Data, _data1_);
#line 237 "individual-zeitgeist.c"
	}
}


static Block2Data* block2_data_ref (Block2Data* _data2_) {
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	g_atomic_int_inc (&_data2_->_ref_count_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	return _data2_;
#line 247 "individual-zeitgeist.c"
}


static void block2_data_unref (void * _userdata_) {
	Block2Data* _data2_;
	_data2_ = (Block2Data*) _userdata_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	if (g_atomic_int_dec_and_test (&_data2_->_ref_count_)) {
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_g_object_unref0 (_data2_->i);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		block1_data_unref (_data2_->_data1_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_data2_->_data1_ = NULL;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		g_slice_free (Block2Data, _data2_);
#line 264 "individual-zeitgeist.c"
	}
}


static void ____lambda5_ (Block2Data* _data2_) {
	Block1Data* _data1_;
	FolksIndividual* _tmp0_;
#line 71 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_data1_ = _data2_->_data1_;
#line 71 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp0_ = _data2_->i;
#line 71 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	print_individual (_tmp0_);
#line 278 "individual-zeitgeist.c"
}


static void _____lambda5__g_object_notify (GObject* _sender, GParamSpec* pspec, gpointer self) {
#line 71 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	____lambda5_ (self);
#line 285 "individual-zeitgeist.c"
}


static void ____lambda6_ (Block2Data* _data2_) {
	Block1Data* _data1_;
	FolksIndividual* _tmp0_;
#line 72 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_data1_ = _data2_->_data1_;
#line 72 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp0_ = _data2_->i;
#line 72 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	print_individual (_tmp0_);
#line 298 "individual-zeitgeist.c"
}


static void _____lambda6__g_object_notify (GObject* _sender, GParamSpec* pspec, gpointer self) {
#line 72 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	____lambda6_ (self);
#line 305 "individual-zeitgeist.c"
}


static void __lambda4_ (Block1Data* _data1_) {
	{
		GeeIterator* _i_it;
		FolksIndividualAggregator* _tmp0_;
		GeeMap* _tmp1_;
		GeeMap* _tmp2_;
		GeeCollection* _tmp3_;
		GeeCollection* _tmp4_;
		GeeCollection* _tmp5_;
		GeeIterator* _tmp6_;
		GeeIterator* _tmp7_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp0_ = _data1_->aggregator;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp1_ = folks_individual_aggregator_get_individuals (_tmp0_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp2_ = _tmp1_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp3_ = gee_map_get_values (_tmp2_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp4_ = _tmp3_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp5_ = _tmp4_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp6_ = gee_iterable_iterator ((GeeIterable*) _tmp5_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp7_ = _tmp6_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_g_object_unref0 (_tmp5_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_i_it = _tmp7_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		while (TRUE) {
#line 342 "individual-zeitgeist.c"
			Block2Data* _data2_;
			GeeIterator* _tmp8_;
			gboolean _tmp9_;
			GeeIterator* _tmp10_;
			gpointer _tmp11_;
			guint count;
			FolksIndividual* _tmp12_;
			guint _tmp13_;
			guint _tmp14_;
			guint call_count;
			FolksIndividual* _tmp15_;
			guint _tmp16_;
			guint _tmp17_;
			GDateTime* chat_datetime;
			FolksIndividual* _tmp18_;
			GDateTime* _tmp19_;
			GDateTime* _tmp20_;
			GDateTime* _tmp21_;
			GDateTime* call_datetime;
			FolksIndividual* _tmp22_;
			GDateTime* _tmp23_;
			GDateTime* _tmp24_;
			GDateTime* _tmp25_;
			guint _tmp26_;
			FolksIndividual* _tmp42_;
			FolksIndividual* _tmp43_;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_data2_ = g_slice_new0 (Block2Data);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_data2_->_ref_count_ = 1;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_data2_->_data1_ = block1_data_ref (_data1_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp8_ = _i_it;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp9_ = gee_iterator_next (_tmp8_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			if (!_tmp9_) {
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				block2_data_unref (_data2_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_data2_ = NULL;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				break;
#line 387 "individual-zeitgeist.c"
			}
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp10_ = _i_it;
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp11_ = gee_iterator_get (_tmp10_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_data2_->i = (FolksIndividual*) _tmp11_;
#line 56 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp12_ = _data2_->i;
#line 56 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp13_ = folks_interaction_details_get_im_interaction_count ((FolksInteractionDetails*) _tmp12_);
#line 56 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp14_ = _tmp13_;
#line 56 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			count = _tmp14_;
#line 57 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp15_ = _data2_->i;
#line 57 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp16_ = folks_interaction_details_get_call_interaction_count ((FolksInteractionDetails*) _tmp15_);
#line 57 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp17_ = _tmp16_;
#line 57 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			call_count = _tmp17_;
#line 58 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp18_ = _data2_->i;
#line 58 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp19_ = folks_interaction_details_get_last_im_interaction_datetime ((FolksInteractionDetails*) _tmp18_);
#line 58 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp20_ = _tmp19_;
#line 58 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp21_ = _g_date_time_ref0 (_tmp20_);
#line 58 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			chat_datetime = _tmp21_;
#line 59 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp22_ = _data2_->i;
#line 59 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp23_ = folks_interaction_details_get_last_call_interaction_datetime ((FolksInteractionDetails*) _tmp22_);
#line 59 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp24_ = _tmp23_;
#line 59 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp25_ = _g_date_time_ref0 (_tmp24_);
#line 59 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			call_datetime = _tmp25_;
#line 60 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp26_ = count;
#line 60 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			if (_tmp26_ > ((guint) 0)) {
#line 435 "individual-zeitgeist.c"
				gint64 timestamp;
				GDateTime* _tmp27_;
				FILE* _tmp30_;
				FolksIndividual* _tmp31_;
				const gchar* _tmp32_;
				const gchar* _tmp33_;
				guint _tmp34_;
				gint64 _tmp35_;
				GDateTime* _tmp36_;
				FILE* _tmp39_;
				guint _tmp40_;
				gint64 _tmp41_;
#line 62 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				timestamp = (gint64) 0;
#line 63 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp27_ = chat_datetime;
#line 63 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				if (_tmp27_ != NULL) {
#line 454 "individual-zeitgeist.c"
					GDateTime* _tmp28_;
					gint64 _tmp29_;
#line 64 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
					_tmp28_ = chat_datetime;
#line 64 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
					_tmp29_ = g_date_time_to_unix (_tmp28_);
#line 64 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
					timestamp = _tmp29_;
#line 463 "individual-zeitgeist.c"
				}
#line 65 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp30_ = stdout;
#line 65 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp31_ = _data2_->i;
#line 65 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp32_ = folks_alias_details_get_alias ((FolksAliasDetails*) _tmp31_);
#line 65 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp33_ = _tmp32_;
#line 65 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp34_ = count;
#line 65 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp35_ = timestamp;
#line 65 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				fprintf (_tmp30_, "\n" \
" %s\n" \
"   chat interaction count: %u\n" \
"   chat interaction timestamp: %lld\n", _tmp33_, _tmp34_, _tmp35_);
#line 66 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				timestamp = (gint64) 0;
#line 67 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp36_ = call_datetime;
#line 67 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				if (_tmp36_ != NULL) {
#line 485 "individual-zeitgeist.c"
					GDateTime* _tmp37_;
					gint64 _tmp38_;
#line 68 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
					_tmp37_ = call_datetime;
#line 68 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
					_tmp38_ = g_date_time_to_unix (_tmp37_);
#line 68 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
					timestamp = _tmp38_;
#line 494 "individual-zeitgeist.c"
				}
#line 69 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp39_ = stdout;
#line 69 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp40_ = call_count;
#line 69 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				_tmp41_ = timestamp;
#line 69 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
				fprintf (_tmp39_, "   call interaction count: %u\n   call interaction timestamp: %lld\n", _tmp40_, _tmp41_);
#line 504 "individual-zeitgeist.c"
			}
#line 71 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp42_ = _data2_->i;
#line 71 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			g_signal_connect_data ((GObject*) _tmp42_, "notify::im-interaction-count", (GCallback) _____lambda5__g_object_notify, block2_data_ref (_data2_), (GClosureNotify) block2_data_unref, 0);
#line 72 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_tmp43_ = _data2_->i;
#line 72 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			g_signal_connect_data ((GObject*) _tmp43_, "notify::call-interaction-count", (GCallback) _____lambda6__g_object_notify, block2_data_ref (_data2_), (GClosureNotify) block2_data_unref, 0);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_g_date_time_unref0 (call_datetime);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_g_date_time_unref0 (chat_datetime);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			block2_data_unref (_data2_);
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
			_data2_ = NULL;
#line 522 "individual-zeitgeist.c"
		}
#line 54 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_g_object_unref0 (_i_it);
#line 526 "individual-zeitgeist.c"
	}
}


static void ___lambda4__g_object_notify (GObject* _sender, GParamSpec* pspec, gpointer self) {
#line 52 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	__lambda4_ (self);
#line 534 "individual-zeitgeist.c"
}


static void __lambda8_ (Block1Data* _data1_, GObject* s, GAsyncResult* r) {
	GError * _inner_error_ = NULL;
#line 78 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	g_return_if_fail (r != NULL);
#line 542 "individual-zeitgeist.c"
	{
		FolksIndividualAggregator* _tmp0_;
		GAsyncResult* _tmp1_;
#line 82 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp0_ = _data1_->aggregator;
#line 82 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp1_ = r;
#line 82 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		folks_individual_aggregator_prepare_finish (_tmp0_, _tmp1_, &_inner_error_);
#line 82 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 554 "individual-zeitgeist.c"
			goto __catch0_g_error;
		}
	}
	goto __finally0;
	__catch0_g_error:
	{
		GError* e1 = NULL;
		const gchar* _tmp2_;
#line 80 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		e1 = _inner_error_;
#line 80 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_inner_error_ = NULL;
#line 86 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_tmp2_ = e1->message;
#line 86 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		g_critical ("individual-zeitgeist.vala:86: Failed to prepare aggregator: %s", _tmp2_);
#line 87 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		g_assert_not_reached ();
#line 80 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		_g_error_free0 (e1);
#line 575 "individual-zeitgeist.c"
	}
	__finally0:
#line 80 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 80 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 80 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		g_clear_error (&_inner_error_);
#line 80 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
		return;
#line 586 "individual-zeitgeist.c"
	}
}


static void ___lambda8__gasync_ready_callback (GObject* source_object, GAsyncResult* res, gpointer self) {
#line 78 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	__lambda8_ (self, source_object, res);
#line 78 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	block1_data_unref (self);
#line 596 "individual-zeitgeist.c"
}


static gboolean __lambda7_ (Block1Data* _data1_) {
	gboolean result = FALSE;
	FolksIndividualAggregator* _tmp0_;
#line 78 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp0_ = _data1_->aggregator;
#line 78 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	folks_individual_aggregator_prepare (_tmp0_, ___lambda8__gasync_ready_callback, block1_data_ref (_data1_));
#line 90 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	result = FALSE;
#line 90 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	return result;
#line 611 "individual-zeitgeist.c"
}


static gboolean ___lambda7__gsource_func (gpointer self) {
	gboolean result;
	result = __lambda7_ (self);
#line 76 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	return result;
#line 620 "individual-zeitgeist.c"
}


gint _vala_main (gchar** args, int args_length1) {
	gint result = 0;
	Block1Data* _data1_;
	GMainLoop* main_loop;
	GMainLoop* _tmp0_;
	FolksIndividualAggregator* _tmp1_;
	FolksIndividualAggregator* _tmp2_;
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_data1_ = g_slice_new0 (Block1Data);
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_data1_->_ref_count_ = 1;
#line 48 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp0_ = g_main_loop_new (NULL, FALSE);
#line 48 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	main_loop = _tmp0_;
#line 50 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp1_ = folks_individual_aggregator_dup ();
#line 50 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_data1_->aggregator = _tmp1_;
#line 52 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_tmp2_ = _data1_->aggregator;
#line 52 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	g_signal_connect_data ((GObject*) _tmp2_, "notify::is-quiescent", (GCallback) ___lambda4__g_object_notify, block1_data_ref (_data1_), (GClosureNotify) block1_data_unref, 0);
#line 76 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	g_idle_add_full (G_PRIORITY_DEFAULT_IDLE, ___lambda7__gsource_func, block1_data_ref (_data1_), block1_data_unref);
#line 94 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	g_main_loop_run (main_loop);
#line 97 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_g_object_unref0 (_data1_->aggregator);
#line 97 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_data1_->aggregator = NULL;
#line 99 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	result = 0;
#line 99 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_g_main_loop_unref0 (main_loop);
#line 99 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	block1_data_unref (_data1_);
#line 99 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	_data1_ = NULL;
#line 99 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	return result;
#line 665 "individual-zeitgeist.c"
}


int main (int argc, char ** argv) {
#if !GLIB_CHECK_VERSION (2,35,0)
	g_type_init ();
#endif
#line 46 "/opt/gnome/source/folks/tests/telepathy/individual-zeitgeist.vala"
	return _vala_main (argv, argc);
#line 675 "individual-zeitgeist.c"
}



