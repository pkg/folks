/* tpf-test.vapi generated by valac 0.35.3.10-6b27, do not modify. */

namespace TpfTest {
	[CCode (cheader_filename = "tpf-test.h")]
	public class MixedTestCase : TpfTest.TestCase {
		public MixedTestCase (string name);
		public override bool use_keyfile_too { get; }
	}
	[CCode (cheader_filename = "tpf-test.h")]
	public class TestCase : Folks.TestCase {
		public void* account_handle;
		public KfTest.Backend? kf_backend;
		public TpTests.Backend? tp_backend;
		public TestCase (string name);
		public virtual void create_kf_backend ();
		public virtual void create_tp_backend ();
		public override void set_up ();
		public virtual void set_up_kf ();
		public virtual void set_up_tp ();
		public override void tear_down ();
		public virtual bool use_keyfile_too { get; }
		public override bool uses_dbus_1 { get; }
	}
}
